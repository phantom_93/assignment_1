﻿using System;

namespace assignment_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name"); 
            string yourName = Console.ReadLine(); 
            Console.WriteLine($"Hello {yourName}, your name is {yourName.Length} characters long and starts with a {yourName[0]}.");
        }
    }
}
